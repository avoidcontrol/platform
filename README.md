## Project Architecture:

- `listenBot`
  - Event driven, streaming interface to Twitter
  - A post to the configured channel triggers the bot to attempt to find a station name in the tweet. If a station is found it creates an incident.
  - The tweet and associated incident is stored in a mapbox dataset
- `postbot`
  - Runs chronologically
  - Makes twitter posts with text and eventually visualisations.

## Development Environment

When developing locally, the goal is to be able to develop each service independent of all other services. Tests on the input and output from the interfaces of the service are integral to achieving this goal. Each service has it's own readme, but generally the process for starting the dev environment is the same for each service.

1. `cd` in to the directory

2. Copy `.env_template` and rename to `.env`, fill in the values (See **Environment variables** section)

3. `npm i`

4. `npm run start`

### Testing

Inside each directory you can run:

```
npm run test -- --watchAll
```

### Linting and formatting

Inside each directory you can run:

```
npm run lint
```

Linting is configured for Typescript and Prettier.

The entire project has same linting rules. CI is configured to lint the entire project on each push.

Formatting uses Prettier. The custom config is in the file `.prettierrc`. It's recommended to configure your text editor to format on save using Prettier and the configuration file.

### Environment variables

Each service which requires environment variables includes a `.env_template` file. The entire project also includes a `.env_template` which contains all of the env variables required in every service. This is only needed for docker. For running locally, if needed, copy the service's `.env_template` and fill in the required values. See the API section of this doc, or each service's readme to see more about what environment variables are needed.

## Production

This project uses docker-compose.

1. Install docker and docker-compose
2. Copy `.env_template` and rename to `.env`, fill in the values
3. `docker-compose up`

## APIS

### Mapbox

Mapbox is used for storing tweets. Tests are not dependent on mapbox.

Mapbox uses:

- Incidents dataset. All instances are stored here. The props interface is in `lib/types.ts`.
  [ Need to document which datasets are used and for what]

### Twitter

[ Need to document which accounts are used and for what]
