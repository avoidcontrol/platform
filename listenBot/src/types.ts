export interface NameAndId {
  name: string;
  id: string;
}

export interface IncidentProps {
  station: NameAndId;
  direction?: NameAndId;
  line?: NameAndId;
  timestamp: string;
  text: string;
}

export interface StationProps {
  name: string;
  longName: string;
  altNames: string[];
  id: string;
}
