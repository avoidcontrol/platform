export const logString = (title: string, message?: string): string =>
  `${title} (${Math.floor(Date.now())})${message ? `: ${message}` : ''}`;
