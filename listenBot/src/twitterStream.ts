import Twitter from 'twitter-lite';
import dotenv from 'dotenv';

import { logString } from './logging';

import { mapboxAPIAddIncident } from './api/mapbox';
import { createIncident } from './geoparse/incident';

dotenv.config();

export interface Tweet {
  created_at: string;
  timestamp_ms: string;
  text?: string;
  extended_tweet?: { full_text: string };
}

const getTweetText = (data: Tweet): string => {
  if (data.extended_tweet) {
    return data.extended_tweet.full_text;
  }
  return data.text;
};

const handlePost = async (data: Tweet): Promise<void> => {
  // Get relevant data
  const text = getTweetText(data);
  const { timestamp_ms: timestamp } = data;

  // Create incident
  const incident = createIncident({ text, timestamp });

  // If incident has station in tweet
  if (incident) {
    // Try to save to mapbox dataset
    try {
      const feature = await mapboxAPIAddIncident(incident);
      // Log successful storage of incident to DB.
      console.log(
        logString(
          'FEATURE ADDED',
          `featureId: ${String(feature.id)} station: ${
            feature.properties.station.name
          }) text: ${feature.properties.text}`,
        ),
      );
    } catch (err) {
      console.error(logString('ERROR SAVING INCIDENT'));
    }
  } else {
    console.log(logString('TWEET WITHOUT INCIDENT', text));
  }
};

/**
 * Twitter API Stream
 * Follows the ticketlos berlin account activity and when a new tweet is shared, it
 * geolocates the incident. For all tweets it adds them to the database, in case
 * geolocation fails to have more complete data.
 */
export const openTwitterStream = async (client: Twitter): Promise<void> => {
  const { NOTICKET_ACCOUNT_ID } = process.env;

  const streamingParameters = {
    follow: NOTICKET_ACCOUNT_ID,
  };

  // Log startup
  console.log(logString('NOTICKET_ACCOUNT_ID', NOTICKET_ACCOUNT_ID));

  client
    .stream('statuses/filter', streamingParameters)
    .on('start', () => console.log(logString('STREAM OPENED')))
    // When tweet is recieved
    .on('data', async (tweet: Tweet) => await handlePost(tweet))
    .on('error', (error: any) => console.log(error))
    .on('end', () => console.log(logString('STREAM CLOSED')));
};
