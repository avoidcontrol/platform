import { createIncident } from './incident';

describe('incident.ts', () => {
  describe('createIncident', () => {
    let examplePost;

    beforeAll(() => {
      examplePost = {
        timestamp: '1584653066272',
        text: 'This text contains no station',
      };
    });

    it('returns null when no incident found', () => {
      const incident = createIncident(examplePost);
      expect(incident).toEqual(null);
    });

    it('returns an incident with the station from the text', () => {
      examplePost.text = 'Schönefeld airport';
      const incident = createIncident(examplePost);
      expect(incident.properties.station.name).toEqual(
        'Flughafen Berlin-Schönefeld',
      );
      expect(incident.properties.station.id).toEqual('900000260005');
    });

    it('returns an incident with station and line from the text', () => {
      examplePost.text = 'leinestrasse Linie U8';
      const incident = createIncident(examplePost);
      expect(incident.properties.line.name).toEqual('U8');
      expect(incident.properties.line.id).toEqual(null);
      expect(incident.properties.station.name).toEqual('Leinestr');
    });

    it('returns an incident with direction station after richtung', () => {
      examplePost.text = 'Gerade westhafen richtung Turmstrasse auf Linie U9';
      const incident = createIncident(examplePost);
      expect(incident.properties.direction.name).toBe('Turmstr');
      expect(incident.properties.line.name).toEqual('U9');
    });

    it('returns incident geojson with all expected properties', () => {
      examplePost.text = 'U8 alex richtung hermannplatz gibts kontroletti';
      const incident = createIncident(examplePost);
      expect(incident.properties.station.name).toBe('Alexanderplatz');
      expect(incident.properties.line.name).toEqual('U8');
      expect(incident.properties.direction.name).toEqual('Hermannplatz');
      expect(incident.properties.timestamp).toEqual('1584653066272');
      expect(incident.properties.text).toEqual(
        'U8 alex richtung hermannplatz gibts kontroletti',
      );
    });
  });
});
