import inputData from './labelledTweets.json';
import {
  findStationInContext,
  findDirectionInContext,
  findLineInText,
} from '../incident';
import {
  generateStationComparisons,
  generateLineComparisons,
  getNumberPredictedCorrect,
  getNumberPredictedTruePositives,
  getNumberPredictedAllPositives,
  getNumberPredictedFalseNegAndTruePositives,
} from './utils';

/**
 * Statistical analysis of the model for identifying the station, line, and direction
 * in tweets. This is used as a testbed for improvements to the performance of the model.
 * There may be a time to start to test the individual functions, but right now the function
 * to test is:
 *
 * createIncident()
 *
 */
describe.skip('Performance of model', () => {
  it('when identifying the station in a tweet', () => {
    const stationPredictions = inputData.map(labelledTweet => {
      const { text } = labelledTweet;
      // Attempt to create incident
      return findStationInContext(text);
    });

    const comparisons = generateStationComparisons(
      'station',
      stationPredictions,
      inputData,
    );

    // Count number with correct true
    const numberPredictedCorrect = getNumberPredictedCorrect(
      'station',
      comparisons,
    );

    const accuracy = numberPredictedCorrect / inputData.length;

    const numberPredictedTruePositives = getNumberPredictedTruePositives(
      comparisons,
    );

    const numberPredictedAllPositives = getNumberPredictedAllPositives(
      comparisons,
    );

    const precision =
      numberPredictedTruePositives / numberPredictedAllPositives;

    // Recall
    const numberPredictedFalseNegAndTruePositives = getNumberPredictedFalseNegAndTruePositives(
      comparisons,
    );

    const recall =
      numberPredictedTruePositives / numberPredictedFalseNegAndTruePositives;

    console.log(`
        Station identification model

        Accuracy: ${accuracy}
        Precision: ${precision}
        Recall: ${recall}
      `);
  });

  it('when identifying the direction station in a tweet', () => {
    const predictions = inputData.map(labelledTweet => {
      const { text } = labelledTweet;
      // Attempt to create incident
      return findDirectionInContext(text);
    });

    const comparisons = generateStationComparisons(
      'direction',
      predictions,
      inputData,
    );

    // Count number with correct true
    const numberPredictedCorrect = getNumberPredictedCorrect(
      'direction',
      comparisons,
    );

    const accuracy = numberPredictedCorrect / inputData.length;

    const numberPredictedTruePositives = getNumberPredictedTruePositives(
      comparisons,
    );

    const numberPredictedAllPositives = getNumberPredictedAllPositives(
      comparisons,
    );

    const precision =
      numberPredictedTruePositives / numberPredictedAllPositives;

    // Recall
    const numberPredictedFalseNegAndTruePositives = getNumberPredictedFalseNegAndTruePositives(
      comparisons,
    );

    const recall =
      numberPredictedTruePositives / numberPredictedFalseNegAndTruePositives;

    console.log(`
        Direction identification model

        Accuracy: ${accuracy}
        Precision: ${precision}
        Recall: ${recall}
      `);
  });

  it('when identifying the line in a tweet', () => {
    const predictions = inputData.map(labelledTweet => {
      const { text } = labelledTweet;
      // Attempt to create incident
      return findLineInText(text);
    });

    const comparisons = generateLineComparisons('line', predictions, inputData);

    // Count number with correct true
    const numberPredictedCorrect = getNumberPredictedCorrect(
      'line',
      comparisons,
    );

    const accuracy = numberPredictedCorrect / inputData.length;

    const numberPredictedTruePositives = getNumberPredictedTruePositives(
      comparisons,
    );

    const numberPredictedAllPositives = getNumberPredictedAllPositives(
      comparisons,
    );

    const precision =
      numberPredictedTruePositives / numberPredictedAllPositives;

    // Recall
    const numberPredictedFalseNegAndTruePositives = getNumberPredictedFalseNegAndTruePositives(
      comparisons,
    );

    const recall =
      numberPredictedTruePositives / numberPredictedFalseNegAndTruePositives;

    console.log(`
        Line identification model

        Accuracy: ${accuracy}
        Precision: ${precision}
        Recall: ${recall}
      `);
  });
});
