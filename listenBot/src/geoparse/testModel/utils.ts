import { Feature, Point } from '@turf/helpers';
import { StationProps, NameAndId } from '../../types';

// [
//   { station: true, correct: true }, // true positive
//   { station: false, correct: true }, // true negative
//   { station: false, correct: false }, // false negative
//   { station: true, correct: false }, // false positive
// ];

export interface LabelledTweet {
  text: string;
  station?: string;
  direction?: string;
  line?: string;
}

export interface ComparisonObject {
  isNull: boolean;
  correct: boolean;
}

type ResultsComparisonKey = 'station' | 'direction' | 'line';

export const generateStationComparisons = (
  key: ResultsComparisonKey,
  predictions: Array<Feature<Point, StationProps> | null>,
  inputData: LabelledTweet[],
): ComparisonObject[] => {
  return predictions.map((prediction, index) => {
    if (!prediction) {
      return {
        isNull: true,
        correct: inputData[index][key] == null,
      };
    }
    // If prediction
    return {
      isNull: false,
      // Check that predicted station matches the labelled station
      correct: prediction.properties.id === inputData[index][key],
    };
  });
};

export const generateLineComparisons = (
  key: ResultsComparisonKey,
  predictions: Array<NameAndId | null>,
  inputData: LabelledTweet[],
): ComparisonObject[] => {
  return predictions.map((prediction, index) => {
    if (!prediction) {
      return {
        isNull: true,
        correct: inputData[index][key] == null,
      };
    }
    // If prediction
    return {
      isNull: false,
      // Check that predicted station matches the labelled station
      correct: prediction.name === inputData[index][key],
    };
  });
};

export const getNumberPredictedCorrect = (
  key: ResultsComparisonKey,
  comparisons: ComparisonObject[],
): number => {
  const correct = comparisons.filter(comparison => {
    return comparison.correct;
  });
  return correct.length;
};

export const getNumberPredictedTruePositives = (
  comparisons: ComparisonObject[],
): number => {
  const truePositives = comparisons.filter(comparison => {
    return !comparison.isNull && comparison.correct;
  });
  return truePositives.length;
};

export const getNumberPredictedAllPositives = (
  comparisons: ComparisonObject[],
): number => {
  const allPositives = comparisons.filter(comparison => {
    return !comparison.isNull;
  });
  return allPositives.length;
};

export const getNumberPredictedFalseNegAndTruePositives = (
  comparisons: ComparisonObject[],
): number => {
  const falseNegativesAndTruePositives = comparisons.filter(comparison => {
    return (
      (!comparison.isNull && comparison.correct) ||
      // If no prediction and is incorrect
      (comparison.isNull && !comparison.correct)
    );
  });
  return falseNegativesAndTruePositives.length;
};
