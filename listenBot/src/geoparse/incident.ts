import { point, Point, Feature } from '@turf/helpers';
import { findStationInText } from './station';
import { IncidentProps, NameAndId, StationProps } from '../types';

import lineList from '../resources/s_u_lines.json';

const wordsAfterDirectionWord = (text: string): string => {
  // Lowercase the text
  const lowerText = text.toLowerCase();

  let word = '';
  // If the tweet includes the words "nach" or "richtung", or uses arrow ">", set keyword
  if (lowerText.includes('richtung')) word = 'richtung';
  else if (lowerText.includes('ri.')) word = 'ri.';
  else if (lowerText.includes('nach')) word = 'nach';
  else if (lowerText.includes('>')) word = '>';
  else if (lowerText.includes('direction')) word = 'direction';
  else if (lowerText.includes('going to')) word = 'going to';

  // If direction word in tweet
  if (word.length > 0) {
    // Slice text
    const split = lowerText.split(word);
    const after = split[1];
    const afterArray = after.split(' ');
    const twoWords = afterArray.slice(1, 3);
    const words = twoWords.join(' ');
    // Return the words
    return words;
  } else return '';
};

export const findStationInContext = (
  text: string,
): Feature<Point, StationProps> => {
  const lowerText = text.toLowerCase();
  // Remove two words after direction words
  const wordsAfter = wordsAfterDirectionWord(text);
  if (wordsAfter.length > 0) {
    // Remove words after direction words
    const splitText = lowerText.split(wordsAfter);
    const wordList = splitText.reduce((acc, value) => {
      const partWordList = value.split(' ');
      return [...acc, ...partWordList];
    }, []);
    const newText = wordList.join(' ');
    // Check if matching station in text without words after direction words
    return findStationInText(newText);
  } else {
    // Check if matching station
    return findStationInText(lowerText);
  }
};

export const findLineInText = (text: string): NameAndId | null => {
  const lowerText = text.toLowerCase();

  let foundLine: string;
  for (const line of lineList) {
    if (lowerText.includes(line.toLowerCase())) {
      foundLine = line;
      break;
    }
  }
  // Return nothing if no line found
  if (!foundLine) return null;

  const result = {
    name: foundLine,
    id: null,
  };
  return result;
};

// Identify direction indicated
export const findDirectionInContext = (
  text: string,
): Feature<Point, StationProps> | null => {
  const twoWords = wordsAfterDirectionWord(text);
  return findStationInText(twoWords);
};

export const createIncident = ({
  text,
  timestamp,
}: {
  text: string;
  timestamp: string;
}): Feature<Point, IncidentProps> => {
  // Geoparse text
  const station = findStationInContext(text);
  const direction = findDirectionInContext(text);
  const line = findLineInText(text);

  // If no station, do not create incident
  if (station == null) return null;

  // Build incident object
  const incident = point(
    // Geometry
    station.geometry.coordinates,
    // Properties
    {
      station: {
        name: station.properties.name,
        id: station.properties.id as string,
      },
      direction: direction
        ? {
            name: direction.properties.name,
            id: direction.properties.id as string,
          }
        : null,
      line: line
        ? {
            name: line.name,
            id: line.id,
          }
        : null,
      timestamp,
      text,
    },
    // Id
    {
      id: timestamp,
    },
  );
  return incident;
};
