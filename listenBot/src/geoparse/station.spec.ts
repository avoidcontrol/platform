import { findStationInText } from './station';

describe('station.ts', () => {
  describe('findStationInText', () => {
    it('returns the correct station from text', () => {
      const exampleText = 'Yesterday I went to britz-süd';
      const station = findStationInText(exampleText);
      expect(station.properties.name).toBe('Britz-Süd');
    });

    it('returns the correct station from text with multiple options', () => {
      const exampleText = 'Two pigs at Kottbusser Tor';
      const station = findStationInText(exampleText);
      expect(station.properties.name).toBe('Kottbusser Tor');
    });
  });
});
