import { hashmap, fuzzyparse } from 'fuzzyparse';
import { Feature, Point, FeatureCollection } from '@turf/helpers';

import stationsGeojson from '../resources/s_u_stations_ids_geo.json';
import { StationProps } from '../types';

export const findStationInText = (
  text: string,
): Feature<Point, StationProps> | null => {
  // Returns a station geojson object if a station is identified in the text
  const { features } = stationsGeojson as FeatureCollection<
    Point,
    StationProps
  >;

  const stationsHashmap = hashmap(
    features,
    ['properties', 'name'],
    [['properties', 'altNames']],
  );

  const searchResults = fuzzyparse(stationsHashmap, text);

  return searchResults;
};
