import fetch from 'node-fetch';
import dotenv from 'dotenv';
import { Feature, Point } from '@turf/helpers';
import { IncidentProps } from '../types';

dotenv.config();

const {
  MAPBOX_DATASET_ID_INCIDENTS: incidentsDatasetId,
  MAPBOX_USERNAME: username,
  MAPBOX_TOKEN: accessToken,
} = process.env;

const MAPBOX_API_URI = `https://api.mapbox.com/datasets/v1/`;

export const mapboxAPIAddIncident = async (
  feature: Feature<Point, IncidentProps>,
): Promise<Feature<Point, IncidentProps>> => {
  // Feature id from current time
  const featureId = String(Date.now());

  // Build url
  const builtURL = `${username}/${incidentsDatasetId}/features/${featureId}?access_token=${accessToken}`;
  const url = MAPBOX_API_URI + builtURL;

  // Create feature object to send
  const featureWithId = {
    ...feature,
    id: featureId,
  };

  const reqBody = JSON.stringify(featureWithId);

  // Make request
  const response = await fetch(url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: reqBody,
  });

  // Parse response body
  const respBody: Feature<Point, IncidentProps> = await response.json();

  return respBody;
};
