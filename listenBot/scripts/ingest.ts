import { createIncident } from '../src/geoparse/incident';

import tweets from '../src/resources/tweet_bln.json';

interface Tweet {
  tweet: {
    retweeted: boolean;
    source: string;
    entities: {
      hashtags: string[];
      symbols: string[];
      user_mentions: [
        {
          name: string;
          screen_name: string;
          indices: string[];
          id_str: string;
          id: string;
        },
      ];
      urls: string[];
    };
    display_text_range: string[];
    favorite_count: string;
    id_str: string;
    truncated: boolean;
    retweet_count: string;
    id: string;
    created_at: string;
    favorited: boolean;
    full_text: string;
    lang: string;
  };
}

// @ts-ignore
const incidents = (tweets as Tweet[]).reduce((acc, { tweet }) => {
  // Get relevant data
  const text = tweet.full_text;
  const { created_at: createdAt } = tweet;
  const timestamp = new String(new Date(createdAt).getTime()) as string;

  // Create incident
  const incident = createIncident({ text, timestamp });

  // If incident has station in tweet
  if (incident) {
    return [...acc, incident];
  }
  return acc;
}, []);

console.log(JSON.stringify(incidents, null, '  '));
console.log(incidents.length);
