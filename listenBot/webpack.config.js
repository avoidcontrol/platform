const path = require('path');

module.exports = {
  entry: path.join(__dirname, 'src/server.ts'),
  output: {
    filename: 'index.js',
    path: path.join(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: 'ts-loader',
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      '@': path.resolve(__dirname, '/src'),
    },
  },
  target: 'node',
  node: {
    fs: 'empty',
  },
};
