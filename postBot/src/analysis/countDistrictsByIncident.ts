import {
  Point,
  FeatureCollection,
  Polygon,
  featureCollection,
} from '@turf/helpers';
import booleanPointInPolygon from '@turf/boolean-point-in-polygon';
import { IncidentProps } from '../types';
import districts from '../resources/berlin_city_parts.json';

export const countDistrictsByIncident = (
  incidents: FeatureCollection<Point, IncidentProps>,
) => {
  // Check if incident in district, add it to the count per district
  const countedDistricts = (districts as FeatureCollection<
    Polygon
  >).features.reduce((acc, district) => {
    // Loop through incidents and check if in district, return number of incidents match
    const totalIncidents = incidents.features.reduce((acc, incident) => {
      if (booleanPointInPolygon(incident, district)) {
        return acc + 1;
      }
      return acc;
    }, 0);

    return [
      {
        ...district,
        properties: {
          ...district.properties,
          count: totalIncidents,
        },
      },
      ...acc,
    ];
  }, []);

  const sortedCountedDistricts = countedDistricts.sort((first, second) => {
    if (first.properties.count > second.properties.count) return -1;
    if (first.properties.count < second.properties.count) return 1;
    return 0;
  });

  return featureCollection(sortedCountedDistricts);
};
