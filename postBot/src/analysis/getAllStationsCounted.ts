import { countPointsByProperty } from './countPointsByProperty';
import { featureCollection, FeatureCollection, Point } from '@turf/helpers';
import { IncidentProps, StationProps } from '../types';

interface CountedStationProps {
  name: string;
  longName?: string;
  altNames?: string[];
  id: string;
  count: number;
}

export default (
  stations: FeatureCollection<Point, StationProps>,
  incidents: FeatureCollection<Point, IncidentProps>,
): FeatureCollection<Point, CountedStationProps> => {
  const countedIncidents = countPointsByProperty(
    incidents as FeatureCollection,
    ['station', 'id'],
  );

  // Loop through each station in stations
  const stationsNotInCounted = stations.features.reduce((acc, station) => {
    const stationId = station.properties.id;
    // Check that the station exists in incidents
    const stationExists = countedIncidents.features.reduce(
      (acc, countedStation) => {
        const incidentId = countedStation.properties.station.id;
        if (incidentId === stationId) return true;
        return acc;
      },
      false,
    );
    // If doesn't exist, add it to reducer
    if (!stationExists) {
      // Add count to station
      return [
        ...acc,
        {
          type: station.type,
          geometry: station.geometry,
          properties: {
            name: station.properties.name,
            count: 0,
          },
        },
      ];
    }
    return acc;
  }, []);

  // Convert the incidents to station shape from incident shape
  const convertedIncidents = countedIncidents.features.map(incident => {
    return {
      type: incident.type,
      geometry: incident.geometry,
      properties: {
        name: incident.properties.station.name,
        count: incident.properties.count,
      },
    };
  });

  // Join the 0 count stations with the counted stations
  const allStations = [...stationsNotInCounted, ...convertedIncidents];
  return featureCollection(allStations);
};
