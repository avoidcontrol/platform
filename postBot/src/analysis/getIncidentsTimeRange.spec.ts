import getIncidentsTimeRange from './getIncidentsTimeRange';
import { Feature, Point } from '@turf/helpers';

import { IncidentProps } from '../types';

const mockFeatures = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      id: '1584083380000',
      properties: {
        station: {
          name: 'Berliner Str',
          id: '900000044201',
        },
        direction: null,
        line: {
          name: 'U9',
          id: null,
        },
        timestamp: '1584083380000',
        text:
          'RT @fickDieNSAFD: #noticket_bln\nU9 Berliner Straße in Richtung Osloer',
      },
      geometry: {
        type: 'Point',
        coordinates: [13.331355, 52.487047],
      },
    },
  ],
};

describe('getIncidentsTimeRange', () => {
  it('returns incident within time boundaries', () => {
    const results = getIncidentsTimeRange(
      mockFeatures.features as Feature<Point, IncidentProps>[],
      1584083379999,
      1584083380001,
    );
    expect(results.length).toEqual(1);
  });
});
