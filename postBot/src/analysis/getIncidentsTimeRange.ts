import { Feature, Point } from '@turf/helpers';

import { IncidentProps } from '../types';

// type OneOfManyDates = Date | string | number;
type OneOfManyDates = number;

export default (
  incidents: Feature<Point, IncidentProps>[],
  minTime: OneOfManyDates,
  maxTime: OneOfManyDates,
) => {
  return incidents.filter(incident => {
    const unixtime = parseInt(incident.properties.timestamp);
    return unixtime > minTime && unixtime < maxTime;
  });
};
