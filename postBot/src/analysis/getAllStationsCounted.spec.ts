import {
  point,
  featureCollection,
  FeatureCollection,
  Point,
} from '@turf/helpers';
import getAllStationsCounted from './getAllStationsCounted';
import { IncidentProps, StationProps } from '../types';

const station1Props = {
  name: 'station 1',
  id: '1234',
};

const station2Props = {
  name: 'station 2',
  id: '1234',
};
const stationFeatures = [
  point([12, 12], station1Props),
  point([13, 12], station2Props),
];

const stations = featureCollection(stationFeatures);

const incidentProps = {
  station: {
    name: 'station2',
    id: '4321',
  },
  timestamp: '1234567',
  text: 'hewwo',
};

const incidentFeatures = [
  point([10, 10], incidentProps),
  point([10, 10], incidentProps),
  point([4, 4], incidentProps),
];

const incidents = featureCollection(incidentFeatures);

describe.skip('getAllStationsCounted.ts', () => {
  describe('getAllStationsCounted', () => {
    it('takes stations and incidents, and creates counts of incidents at all stations', () => {
      const countedStations = getAllStationsCounted(
        stations as FeatureCollection<Point, StationProps>,
        incidents as FeatureCollection<Point, IncidentProps>,
      );

      expect(countedStations.features.length).toEqual(4);
      expect(
        countedStations.features.every(
          station => typeof station.properties.count === 'number',
        ),
      ).toEqual(true);
      expect(
        countedStations.features.some(station => station.properties.count == 0),
      ).toEqual(true);
      expect(
        countedStations.features.some(station => station.properties.count == 1),
      ).toEqual(true);
      expect(
        countedStations.features.some(station => station.properties.count == 2),
      ).toEqual(true);
      expect(
        countedStations.features.some(station => station.properties.count >= 3),
      ).toEqual(false);
    });
  });
});
