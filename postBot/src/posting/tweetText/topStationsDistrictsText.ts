import { FeatureCollection, Point, Polygon } from '@turf/helpers';
import { IncidentProps } from '../../types';

export const topStationsDistrictsText = (
  isWeekday: boolean,
  countedIncidents: FeatureCollection<Point, IncidentProps & { count: number }>,
  countedDistricts: FeatureCollection,
  time: Date,
): string => {
  const topStationsStrings = countedIncidents.features.map(incident => {
    const { station } = incident.properties;
    return `🚉 ${station.name}`;
  });

  const topDistrictsStrings = countedDistricts.features.map(district => {
    const { OTEIL } = district.properties;
    return `🗺️ ${OTEIL}`;
  });

  const minutesText =
    String(time.getMinutes()).length < 2
      ? `0${time.getMinutes()}`
      : time.getMinutes();

  return `
Most controlled on ${isWeekday ? 'week days' : 'weekend days'}:

Stations:
${topStationsStrings.join('\n')}

Districts:
${topDistrictsStrings.join('\n')}

Since ${time.getHours()}:${minutesText} ${time.getDate()}.${time.getMonth()}.${time.getFullYear()}
  `;
};
