import Twitter from 'twitter-lite';
import { featureCollection } from '@turf/helpers';

import { logString } from '../logging';
import { prevWeeklyRecurringRanges } from '../time/prevWeeklyRecurringRanges';
import { countDistrictsByIncident } from '../analysis/countDistrictsByIncident';
import { countPointsByProperty } from '../analysis/countPointsByProperty';
import { mapboxAPIGetIncidents } from '../api/mapbox';

import { topStationsDistrictsText } from './tweetText/topStationsDistrictsText';
import getIncidentsTimeRange from '../analysis/getIncidentsTimeRange';
import { getNextWeekdayHour } from '../time/getNextWeekdayHour';

export const makePost = async (client: Twitter, now?: Date) => {
  const time = now ? now : new Date();

  console.log(logString('MAKING POST'));
  // Get all incidents
  const incidents = await mapboxAPIGetIncidents();

  // If incidents is not a feature collection
  if (!incidents.type && incidents.type !== 'FeatureCollection') {
    // Log and end process
    console.log(
      logString('ERROR', `Could not get incidents from Mapbox - ${incidents}`),
    );
    return;
  }

  const isWeekday = 0 < time.getDay() && time.getDay() < 6;
  const weekPartRange = isWeekday
    ? {
        start: [1, 0],
        end: [5, 24],
        weeks: 100,
      }
    : {
        start: [6, 0],
        end: [0, 24],
        weeks: 100,
      };

  const ranges = prevWeeklyRecurringRanges(weekPartRange);
  // Get incidents by time range
  const incidentsInRange = ranges
    .map(range => {
      return getIncidentsTimeRange(incidents.features, range[0], range[1]);
    })
    .reduce((acc, featureList) => {
      return [...featureList, ...acc];
    }, []);

  const countedIncidentsByStation = countPointsByProperty(
    featureCollection(incidentsInRange),
    ['station', 'id'],
  );

  const top5ControlledStations = featureCollection(
    countedIncidentsByStation.features.slice(0, 5),
  );

  const topControlledDistricts = countDistrictsByIncident(
    featureCollection(incidentsInRange),
  );

  const top5ControlledDistricts = featureCollection(
    topControlledDistricts.features
      .slice(0, 5)
      .filter(dis => dis.properties.count > 0),
  );

  const tweetText = topStationsDistrictsText(
    isWeekday,
    top5ControlledStations,
    top5ControlledDistricts,
    time,
  );

  try {
    await client.post('statuses/update', {
      status: tweetText,
    });
    console.log(logString('POST MADE'));
    console.log(logString('NEXT POST SCHEDULED', String(14400000)));
    return tweetText;
  } catch (err) {
    console.log(
      logString('ERROR', `Could not post tweet - ${err.errors[0].message}`),
    );
  }
};

/**
 * Posts the most controlled stations from all of the data in a plain text tweet
 * @param client
 */
export const runRecurringPosts = (client: Twitter) => {
  const fourHours = 14400000;
  const postTimes = [4, 8, 12, 16, 20, 24];
  const today = new Date().getDay();

  // Get the next scheduled post time
  const nextPostTime = postTimes.reduce((acc, postTime) => {
    const nextTime = getNextWeekdayHour(today, postTime);
    if (nextTime < acc && nextTime > Date.now()) return nextTime;
    return acc;
  }, Infinity);

  const timeUntilNextPost = nextPostTime - Date.now();
  console.log(logString('NEXT POST SCHEDULED', String(timeUntilNextPost)));

  if (timeUntilNextPost !== Infinity && timeUntilNextPost > 0) {
    // Schedule repeating posts
    setTimeout(() => {
      // Make first post
      setImmediate(makePost, client);

      // Create interval every 4 hours
      setInterval(makePost, fourHours, client);
    }, timeUntilNextPost);
  } else {
    // If there was an issue, just start it now
    setInterval(makePost, fourHours, client);
  }
};
