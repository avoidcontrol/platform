import { makePost } from './runRecurringPosts';

const client = {
  post: (endpoint: string, options: { status: string }) => 'Post made',
};

describe.skip('runRecurringPosts.ts', () => {
  describe('runRecurringPosts', () => {
    it('makes a post', async () => {
      const text = await makePost(client, new Date());
      console.log(text);
    });
  });
});
