import fetch from 'node-fetch';
import dotenv from 'dotenv';
import { Point, FeatureCollection } from '@turf/helpers';
import { IncidentProps } from '../types';
import { logString } from '../logging';

dotenv.config();

const {
  MAPBOX_DATASET_ID_INCIDENTS: incidentsDatasetId,
  MAPBOX_USERNAME: username,
  MAPBOX_TOKEN: accessToken,
} = process.env;

const MAPBOX_API_URI = `https://api.mapbox.com/datasets/v1/`;

export const mapboxAPIGetIncidents = async (): Promise<FeatureCollection<
  Point,
  IncidentProps
>> => {
  // Build url
  const builtURL = `${username}/${incidentsDatasetId}/features/?access_token=${accessToken}`;
  const url = MAPBOX_API_URI + builtURL;

  try {
    // Make request
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });

    // Parse response body
    const respBody: FeatureCollection<
      Point,
      IncidentProps
    > = await response.json();

    return respBody;
  } catch (err) {
    console.log(
      logString('ERROR', `Error loading incidents from Mapbox${err}`),
    );
  }
};
