import { prevWeeklyRecurringRanges } from './prevWeeklyRecurringRanges';

describe('prevWeeklyRecurringRanges.ts', () => {
  describe('prevWeeklyRecurringRanges', () => {
    describe('returns list of list of start and end dates', () => {
      it('when range is in same day and one week requested', () => {
        const now = new Date('17 April 2020 00:00 UTC');
        const rangeObj = {
          // Wednesday 15:00
          start: [3, 15],
          // Wednesday 24:00
          end: [3, 24],
          weeks: 1,
        };

        const results = prevWeeklyRecurringRanges(rangeObj, now);
        const startTime = results[0][0];
        const endTime = results[0][1];

        expect(results.length).toEqual(1);
        expect(new Date(startTime)).toEqual(
          new Date('2020-04-15T15:00:00.000Z'),
        );
        expect(new Date(endTime)).toEqual(new Date('2020-04-15T24:00:00.000Z'));
      });

      it('when range is in different days', () => {
        const now = new Date('17 April 2020 00:00 UTC');
        const rangeObj = {
          // Wednesday 15:00
          start: [2, 12],
          // Wednesday 24:00
          end: [3, 12],
          weeks: 1,
        };

        const results = prevWeeklyRecurringRanges(rangeObj, now);
        const startTime = results[0][0];
        const endTime = results[0][1];

        expect(results.length).toEqual(1);
        expect(new Date(startTime)).toEqual(
          new Date('2020-04-14T12:00:00.000Z'),
        );
        expect(new Date(endTime)).toEqual(new Date('2020-04-15T12:00:00.000Z'));
      });

      it('when range is in same day and multiple weeks requested, but in same month', () => {
        const now = new Date('17 April 2020 00:00 UTC');
        const rangeObj = {
          // Wednesday 15:00
          start: [3, 15],
          // Wednesday 24:00
          end: [3, 24],
          weeks: 3,
        };

        const results = prevWeeklyRecurringRanges(rangeObj, now);

        expect(results.length).toEqual(3);

        const expectedResults = [
          [
            new Date('2020-04-15T15:00:00.000Z'),
            new Date('2020-04-16T00:00:00.000Z'),
          ],
          [
            new Date('2020-04-08T15:00:00.000Z'),
            new Date('2020-04-09T00:00:00.000Z'),
          ],
          [
            new Date('2020-04-01T15:00:00.000Z'),
            new Date('2020-04-02T00:00:00.000Z'),
          ],
        ];

        const parsedResults = results.map((range: number[]) => {
          return [new Date(range[0]), new Date(range[1])];
        });

        expect(expectedResults).toEqual(parsedResults);
      });

      it('when range requested, across months', () => {
        // Friday
        const now = new Date('10 April 2020 00:00 UTC');
        const rangeObj = {
          // Monday 00:00
          start: [1, 0],
          // Monday 24:00
          end: [1, 24],
          weeks: 3,
        };

        const results = prevWeeklyRecurringRanges(rangeObj, now);

        expect(results.length).toEqual(3);

        const expectedResults = [
          [
            new Date('2020-04-06T00:00:00.000Z'),
            new Date('2020-04-07T00:00:00.000Z'),
          ],
          [
            new Date('2020-03-30T00:00:00.000Z'),
            new Date('2020-03-31T00:00:00.000Z'),
          ],
          [
            new Date('2020-03-23T00:00:00.000Z'),
            new Date('2020-03-24T00:00:00.000Z'),
          ],
        ];

        const parsedResults = results.map((range: number[]) => {
          return [new Date(range[0]), new Date(range[1])];
        });

        expect(expectedResults).toEqual(parsedResults);
      });

      it('when start day is bigger than end day', () => {
        // Friday
        const now = new Date('10 April 2020 00:00 UTC');
        const rangeObj = {
          // Wed 12:00
          start: [3, 12],
          // Tuesday 00:00
          end: [2, 0],
          weeks: 3,
        };

        const results = prevWeeklyRecurringRanges(rangeObj, now);

        expect(results.length).toEqual(3);

        const expectedResults = [
          [
            new Date('2020-04-08T12:00:00.000Z'),
            new Date('2020-04-07T00:00:00.000Z'),
          ],
          [
            new Date('2020-04-01T12:00:00.000Z'),
            new Date('2020-04-07T00:00:00.000Z'),
          ],
          [
            new Date('2020-03-25T12:00:00.000Z'),
            new Date('2020-03-31T00:00:00.000Z'),
          ],
        ];

        const parsedResults = results.map((range: number[]) => {
          return [new Date(range[0]), new Date(range[1])];
        });

        expect(expectedResults).toEqual(parsedResults);
      });
    });
  });
});
