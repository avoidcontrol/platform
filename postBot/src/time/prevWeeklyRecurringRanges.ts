import { getLastWeekdayHour } from './getLastWeekdayHour';
interface Range {
  // [day of week, hour]
  start: number[];
  end: number[];
  weeks: number;
}

// [[start, end], [start, end]]
type RangeDates = Array<Array<number>>;

// Get start and end time for all previous dates as many weeks back as specified
export const prevWeeklyRecurringRanges = (
  range: Range,
  currentTime?: Date,
): RangeDates => {
  let now = currentTime != null ? currentTime : new Date();

  const results = [...Array(range.weeks).keys()].map((index: number) => {
    const [startDay, startHour] = range.start;
    const startTimeLast = getLastWeekdayHour(startDay, startHour, now);
    const [endDay, endHour] = range.end;
    const endTimeLast = getLastWeekdayHour(endDay, endHour, now);

    // Overwrite current time in order to move back through time
    now = new Date(startTimeLast);
    // Return start and end times of range
    return [startTimeLast, endTimeLast];
  });
  return results;
};
