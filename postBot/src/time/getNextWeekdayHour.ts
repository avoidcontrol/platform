const getDayDifference = (
  scheduledDay: number,
  weekDay: number,
  scheduledHour: number,
  now: Date,
) => {
  const currentHour = now.getHours();
  // If next scheduled day of week is same, but one week from now
  if (scheduledDay === weekDay && currentHour > scheduledHour) return 7;
  // If next scheduled day is next week
  if (scheduledDay < weekDay) return scheduledDay + (7 - weekDay);
  // If next scheduled day in this week
  return scheduledDay - weekDay;
};

const getNextMonthDate = (
  now: Date,
  monthDay: number,
  dayDifference: number,
) => {
  const currentMonthLength = new Date(
    now.getFullYear(),
    now.getMonth() + 1,
    0,
  ).getDate();
  // If next scheduled date in next month

  if (monthDay + dayDifference > currentMonthLength) {
    const nextMonth = now.getMonth() + 1;
    const nextMonthDay = dayDifference - (currentMonthLength - monthDay);
    return [nextMonth, nextMonthDay];
  }
  const nextMonth = now.getMonth();
  const nextMonthDay = monthDay + dayDifference;

  return [nextMonth, nextMonthDay];
};

export const getNextWeekdayHour = (
  scheduledDay: number,
  scheduledHour: number,
  currentTime?: Date,
) => {
  const now = currentTime != null ? currentTime : new Date();
  const year = now.getFullYear();
  const weekDay = now.getDay();
  const monthDay = now.getDate();

  const dayDifference = getDayDifference(
    scheduledDay,
    weekDay,
    scheduledHour,
    now,
  );

  const [nextMonth, nextMonthDay] = getNextMonthDate(
    now,
    monthDay,
    dayDifference,
  );

  const nextScheduledDate = new Date(
    year,
    nextMonth,
    nextMonthDay,
    scheduledHour,
  );

  return nextScheduledDate.getTime();
};
