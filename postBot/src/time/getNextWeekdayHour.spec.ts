import { getNextWeekdayHour } from './getNextWeekdayHour';

describe('getNextWeekdayHour.ts', () => {
  describe('getNextWeekdayHour', () => {
    describe('returns the time of the next time specified', () => {
      it('when the next one is the same week', () => {
        // Sunday
        const now = new Date('19 April 2020 00:00 UTC');

        // Monday
        const nextDay = 1;
        // 15:00
        const nextHour = 15;

        const nextTime = getNextWeekdayHour(nextDay, nextHour, now);

        expect(new Date(nextTime)).toEqual(
          new Date('2020-04-20T15:00:00.000Z'),
        );
      });

      it('when the next one is the next week', () => {
        // Thursday
        const now = new Date('23 April 2020 00:00 UTC');

        // Monday
        const nextDay = 1;
        // 20:00
        const nextHour = 20;

        const nextTime = getNextWeekdayHour(nextDay, nextHour, now);

        // 20-04-27T20:00:00.000Z
        expect(new Date(nextTime)).toEqual(
          new Date('2020-04-27T20:00:00.000Z'),
        );
      });

      it('when the next one is the next week', () => {
        // Saturday
        const now = new Date('14 March 2020 00:00 UTC');

        // Thursday
        const nextDay = 4;
        // 10
        const nextHour = 10;

        const nextTime = getNextWeekdayHour(nextDay, nextHour, now);

        expect(new Date(nextTime)).toEqual(
          new Date('2020-03-19T10:00:00.000Z'),
        );
      });

      it('when the next one is the next month', () => {
        // Sunday
        const now = new Date('29 March 2020 00:00 UTC');

        // Thursday
        const nextDay = 4;
        // 10
        const nextHour = 10;

        const nextTime = getNextWeekdayHour(nextDay, nextHour, now);

        expect(new Date(nextTime)).toEqual(
          new Date('2020-04-02T10:00:00.000Z'),
        );
      });

      it('when the next one is the next year', () => {
        // Monday
        const now = new Date('31 December 2021 00:00 UTC');

        // Sunday
        const nextDay = 0;
        // 12
        const nextHour = 12;

        const nextTime = getNextWeekdayHour(nextDay, nextHour, now);

        expect(new Date(nextTime)).toEqual(
          new Date('2022-01-02T12:00:00.000Z'),
        );
      });

      it('when the next one is the same day', () => {
        // Thursday
        const now = new Date('9 July 2020 10:00 UTC');

        // Thursday
        const nextDay = 4;
        // 12
        const nextHour = 12;

        const nextTime = getNextWeekdayHour(nextDay, nextHour, now);

        expect(new Date(nextTime)).toEqual(
          new Date('2020-07-09T12:00:00.000Z'),
        );
      });

      it('when the next one is one week from now', () => {
        // Thursday
        const now = new Date('9 July 2020 20:00 UTC');

        // Thursday
        const nextDay = 4;
        // 12
        const nextHour = 12;

        const nextTime = getNextWeekdayHour(nextDay, nextHour, now);

        expect(new Date(nextTime)).toEqual(
          new Date('2020-07-16T12:00:00.000Z'),
        );
      });
    });
  });
});
