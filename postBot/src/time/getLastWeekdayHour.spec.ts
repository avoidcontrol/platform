import { getLastWeekdayHour } from './getLastWeekdayHour';

describe('getLastWeekdayHour.ts', () => {
  describe('getLastWeekdayHour', () => {
    describe('returns the time of the next previous day and time specified ', () => {
      it('when the last one is the same week', () => {
        // Friday
        const now = new Date('17 April 2020 00:00 UTC');

        // Monday
        const prevDay = 1;
        // 15:00
        const prevHour = 15;

        const prevTime = getLastWeekdayHour(prevDay, prevHour, now);

        expect(new Date(prevTime)).toEqual(
          new Date('2020-04-13T15:00:00.000Z'),
        );
      });

      it('another example like above', () => {
        // Monday
        const now = new Date('2020-03-30T12:00:00.000Z');

        // Monday
        const prevDay = 1;
        // 12:00
        const prevHour = 12;

        const prevTime = getLastWeekdayHour(prevDay, prevHour, now);

        expect(new Date(prevTime)).toEqual(
          new Date('2020-03-23T12:00:00.000Z'),
        );
      });

      it('when the last one is the week before', () => {
        // Friday
        const now = new Date('17 April 2020 00:00 UTC');

        // Saturday
        const prevDay = 6;
        // 12:00
        const prevHour = 12;

        const prevTime = getLastWeekdayHour(prevDay, prevHour, now);

        expect(new Date(prevTime)).toEqual(
          new Date('2020-04-11T12:00:00.000Z'),
        );
      });

      it('when the last one is the last month', () => {
        // Tuesday
        const now = new Date('03 March 2020 00:00 UTC');

        // Thursday
        const prevDay = 3;
        // 12
        const prevHour = 12;

        const prevTime = getLastWeekdayHour(prevDay, prevHour, now);

        expect(new Date(prevTime)).toEqual(
          new Date('2020-02-26T12:00:00.000Z'),
        );
      });

      it('when the last one is the last year', () => {
        // Wednesday
        const now = new Date('01 January 2020 00:00 UTC');

        // Thursday
        const prevDay = 2;
        // 12
        const prevHour = 12;

        const prevTime = getLastWeekdayHour(prevDay, prevHour, now);

        expect(new Date(prevTime)).toEqual(
          new Date('2019-12-31T12:00:00.000Z'),
        );
      });

      it('when the last one is the same day', () => {
        // Thursday
        const now = new Date('9 July 2020 12:00 UTC');

        // Thursday
        const prevDay = 4;
        // 12
        const prevHour = 10;

        const prevTime = getLastWeekdayHour(prevDay, prevHour, now);

        expect(new Date(prevTime)).toEqual(
          new Date('2020-07-09T10:00:00.000Z'),
        );
      });

      it('when the last one is one week ago', () => {
        // Thursday
        const now = new Date('9 July 2020 10:00 UTC');

        // Thursday
        const prevDay = 4;
        // 12
        const prevHour = 12;

        const prevTime = getLastWeekdayHour(prevDay, prevHour, now);

        expect(new Date(prevTime)).toEqual(
          new Date('2020-07-02T12:00:00.000Z'),
        );
      });

      it('when it is the exact same time as now', () => {
        // Thursday
        const now = new Date('9 July 2020 10:00 UTC');

        // Thursday
        const prevDay = 4;
        // 12
        const prevHour = 10;

        const prevTime = getLastWeekdayHour(prevDay, prevHour, now);

        expect(new Date(prevTime)).toEqual(
          new Date('2020-07-02T10:00:00.000Z'),
        );
      });
    });
  });
});
