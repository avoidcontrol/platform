const getDayDifference = (
  scheduledDay: number,
  weekDay: number,
  scheduledHour: number,
  now: Date,
) => {
  const currentHour = now.getHours();
  // If next scheduled day of week is same, but one week ago
  if (scheduledDay === weekDay && currentHour <= scheduledHour) return 7;
  // If last prev day is last week
  if (scheduledDay > weekDay) return 7 - scheduledDay + weekDay;
  // If next scheduled day in this week
  return weekDay - scheduledDay;
};

const getLastMonthDate = (
  now: Date,
  monthDay: number,
  dayDifference: number,
) => {
  const prevMonthLength = new Date(
    now.getFullYear(),
    now.getMonth(),
    0,
  ).getDate();
  // If last date in last month
  if (dayDifference > monthDay) {
    const lastMonth = now.getMonth() - 1;
    const lastMonthDay = prevMonthLength - (dayDifference - monthDay);
    return [lastMonth, lastMonthDay];
  }
  const lastMonth = now.getMonth();
  const lastMonthDay = monthDay - dayDifference;
  return [lastMonth, lastMonthDay];
};

export const getLastWeekdayHour = (
  scheduledDay: number,
  scheduledHour: number,
  currentTime?: Date,
) => {
  const now = currentTime != null ? currentTime : new Date();
  const year = now.getFullYear();
  const weekDay = now.getDay();
  const monthDay = now.getDate();

  const dayDifference = getDayDifference(
    scheduledDay,
    weekDay,
    scheduledHour,
    now,
  );

  const [nextMonth, nextMonthDay] = getLastMonthDate(
    now,
    monthDay,
    dayDifference,
  );

  const lastScheduledDate = new Date(
    year,
    nextMonth,
    nextMonthDay,
    scheduledHour,
  );

  return lastScheduledDate.getTime();
};
