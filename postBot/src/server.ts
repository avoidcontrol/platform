import Twitter from 'twitter-lite';

import { runRecurringPosts } from './posting/runRecurringPosts';

import { logString } from './logging';

process.env.TZ = 'Europe/Berlin';

const client: Twitter = new Twitter({
  consumer_key: process.env.TWITTER_CONSUMER_KEY,
  consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
  access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
  access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
});

console.log(logString('STARTED POSTBOT'));
// Run hourly recurring posts
runRecurringPosts(client);
