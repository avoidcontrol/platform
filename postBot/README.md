# twitterListener Service

## Run in development

1. Copy `.env_template` and rename to `.env`, fill in the values

The `NOTICKET_` variables are used to configure which account or hashtag to follow. It's recommended in development to set the hashtag to something other than `#noticket_bln` to not spam @noticket_bln. Set the NOTICKET_ACCOUNT_ID to the account you will use to post debugging tweets from.

**Currently only `NOTICKET_ACCOUNT_ID` is supported**

2. `npm i`

3. `npm run start`

## Scheduling
